let joueur = "1";
let partie_en_cours = true;

let solutions_gagnantes = [
  [[1, 1], [1, 2], [1, 3]],
  [[1, 2], [1, 3], [1, 4]],

  [[2, 1], [2, 2], [2, 3]],
  [[2, 2], [2, 3], [2, 4]],

  [[3, 1], [3, 2], [3, 3]],
  [[3, 2], [3, 3], [3, 4]],

  [[4, 1], [4, 2], [4, 3]],
  [[4, 2], [4, 3], [4, 4]],

  [[1, 1], [2, 1], [3, 1]],
  [[2, 1], [3, 1], [4, 1]],

  [[1, 2], [2, 2], [3, 2]],
  [[2, 2], [3, 2], [4, 2]],

  [[1, 3], [2, 3], [3, 3]],
  [[2, 3], [3, 3], [4, 3]],

  [[1, 4], [2, 4], [3, 4]],
  [[2, 4], [3, 4], [4, 4]],

  [[1, 1], [2, 2], [3, 3]],
  [[2, 2], [3, 3], [4, 4]],
  [[1, 2], [2, 3], [3, 4]],
  [[2, 1], [3, 2], [4, 3]],

  [[4, 1], [3, 2], [2, 3]],
  [[3, 2], [2, 3], [1, 4]],
  [[3, 1], [2, 2], [1, 3]],
  [[4, 2], [3, 3], [2, 4]]
]

function fonctionX(ligne, colonne){
  let postion_id = ligne + "-" + colonne;

  if (partie_en_cours) {
    if (document.getElementById(postion_id).classList.contains("fond-blanc")) {
      document.getElementById(postion_id).classList.remove("fond-blanc");
      if (joueur == "1") {
        document.getElementById(postion_id).classList.add("fond-rouge");
      } else {
        document.getElementById(postion_id).classList.add("fond-bleu");
      }
    }
  }
}

function finPartie() {
  for(var index=0; index < solutions_gagnantes.length; index++) {
    let sous_tableau = solutions_gagnantes[index];
    let tableau_auxiliaire = [];
    for(var index_auxiliaire=0; index_auxiliaire < sous_tableau.length; index_auxiliaire++) {
      let ligne = sous_tableau[index_auxiliaire][0];
      let colonne = sous_tableau[index_auxiliaire][1];
      let postion_id = ligne + "-" + colonne;
      if ((joueur == "1") && (document.getElementById(postion_id).classList.contains("fond-rouge"))) {
        tableau_auxiliaire.push(joueur);
      }
      if ((joueur == "2") && (document.getElementById(postion_id).classList.contains("fond-bleu"))) {
        tableau_auxiliaire.push(joueur);
      }
    }
    if (tableau_auxiliaire.length == 3) {
      if (partie_en_cours) {
        alert("Fin de partie, le joueur " + joueur + " est gagnant!");
        partie_en_cours = false;
      }
    }
  }
  if (joueur == 1) {
    joueur = 2;
  } else {
    joueur = 1;
  }
}
